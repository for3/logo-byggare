.PHONY: all install

all: base.js

%.js: %.jsx
	esbuild '--define:process.env.MODE_ENV="production"' --outfile=$@ --bundle $<

install: all
	install -m644 -Dt $(DESTINATION) index.html
	install -m644 -Dt $(DESTINATION) base.js
