För3 Loggobyggare
=================

<img src="sample-logo.svg"
    align="right"
    width="200"
    alt="För3 exempellogotyp"
    title="Exempellogotyp" />
    
Ett simpelt webb-program för att testbygga logotyper till För3.

En färdigkompilerad version bör finnas tillgänglig på
https://for3.lysator.liu.se/logobyggare

Bygga
-----
    
    npm install
    make
    make install DESTINATION=/tmp
    
Byggberoenden
---------
- npm
- esbuild
