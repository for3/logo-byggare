import * as React from 'react'
import * as ReactDOM from 'react-dom'

const TAU = Math.PI * 2

function trigger_download (string) {
	let blob = new Blob([string], { type: 'image/svg+xml;charset=utf-8' });
	const objectURL = URL.createObjectURL(blob);

	let a = document.createElement('a');
	a.setAttribute('href', objectURL);
	/* Makes link a download link (instead of open in browser),
	 * plus sets sugested filename, */
	a.setAttribute('download', 'for3-logo.svg')
	/* Trigger download */
	a.click();

}

class F extends React.Component {
	render() {
		let sx = - this.props.trunk_width / 2
		let B = this.props.trunk_width / 2
		let r = B / Math.cos(TAU / 12)
		// let sy = Math.sqrt((2*B)**2 - (2*B)**2 / 4) / 2
		let sy = r / 2
		let data = [
			`M ${sx},${sy}`,
			`h ${this.props.trunk_width}`,
			`v ${this.props.trunk_length - (this.props.top_width + this.props.middle_width + this.props.bar_distance)}`,
			`h ${this.props.middle_length}`,
			`v ${this.props.middle_width}`,
			`h -${this.props.middle_length}`,
			`v ${this.props.bar_distance}`,
			`h ${this.props.top_length}`,
			`v ${this.props.top_width}`,
			`h -${this.props.top_length + this.props.trunk_width}`,
			`v -${this.props.trunk_length}`,
			'z'
		]


		let inner = <path
			fill={this.props.foreground}
			stroke={this.props.foreground}
			d={data.join('\n')}/>
		if (this.props.rotate && this.props.rotate != 0) {
			return (<g transform={`rotate(${this.props.rotate})`}>{inner}</g>)
		} else {
			return inner
		}

	}
}

class Logo extends React.Component {
	/*
    <use xlink:href="#F" />
    <use xlink:href="#F" />
	 *
	 *
	 *
	 *
	*/

	render() {
		let fs = []
		for (var i = 0; i < this.props.f_count; i++) {
			fs.push( <F key={i} rotate={i * (360 / this.props.f_count)}    {...this.props}/> )
		}

		let b = this.props.trunk_width
		let sx = b / 2
		let a = Math.sqrt(b**2 - b**2 / 4)
		let sy = (this.props.trunk_width / 2) / Math.cos(TAU / 12) / 2

		let path = [
			`M ${- sx} ${sy}`,
			`h ${2 * sx}`,
			`l ${- b / 2} ${- a}`,
			`z`
		]

		return (
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="-150 -150 300 300"
			     id='thelogo'
			     style={{
					 strokeWidth: 'unset',
					 backgroundColor: this.props.background,
				 }}>
			<g transform='scale(1,-1)'>
				<path fill={this.props.foreground}
				      stroke={this.props.foreground}
					  d={path.join('\n')}/>
				{fs}
			</g>
			</svg>
		)
	}
}

class ColorInput extends React.Component {

	change = (e) => {
		this.props.change(this.props.name, e.target.value);
		e.preventDefault();
	}

	render() {
		return (
			<div>
				<label htmlFor={this.props.name}>{this.props.name}</label><br/>
				<input
					id={this.props.name}
					type="color"
					value={this.props.v}
					onChange={this.change}
				/>
				<input
					type="text"
					value={this.props.v}
					onChange={this.change}
				/>
			</div>
		)
	}
}

class Slider extends React.Component {

	change = (e) => {
		this.props.change(this.props.name, e.target.value);
		e.preventDefault();
	}

	render() {
		return (
			<div>
				<label htmlFor={this.props.name}>{this.props.name}</label><br/>
				<input
					id={this.props.name}
					type="range"
					value={this.props.v}
					onChange={this.change}
				/>
				<input
					type="number"
					value={this.props.v}
					onChange={this.change}
				/>
			</div>
		)
	}
}

class Controlls extends React.Component {

	render () {
		let elements = [];
		for (var item in this.props) {
			if (item == 'change') continue;
			if (item == 'foreground' || item == 'background' ) {
				elements.push(
					<ColorInput
						change={this.props.change}
						key={item}
						name={item}
						v={this.props[item]}
					/>);
				continue;
			}
			elements.push(
				<Slider
					change={this.props.change}
					key={item}
					name={item}
					v={this.props[item]}
				/>);
		}
		return ( <div>{elements}</div> )
	}
}

function parse_type(name, value) {
	switch (name) {
		/* Colors are strings */
		case 'foreground':
		case 'background':
			return value;

		/* Integer conversion for everything else */
		default:
			return +value;
	}
}

class BuildALogo extends React.Component {
	constructor(props) {
		super(props);
		/* Default values for all state */
		this.state = {
			trunk_length: 90,
			trunk_width: 10,
			top_length: 40,
			top_width: 10,
			middle_length: 30,
			middle_width: 10,
			bar_distance: 10,
			f_count: 3,
			foreground: '#000000',
			background: '#FFFFFF',
		};

		/* Which gets overwritten from variables in the hash */
		if (window.location.hash) {
			for (let pair of window.location.hash.substr(1).split('&')) {
				let [key, value] = pair.split('=')
				/* Only allow variables already defined in the state */
				if (this.state[key] !== undefined) {
					this.state[key] = parse_type(key, value);
				}
			}
		}

		window.addEventListener('hashchange', (e) => {
			/* This leads to a double update of the state whenever a
			 * state changes, since we both move our changes through
			 * reacts "regular" methods, but also through the window
			 * URL fragment. Some form of lock could solve this, but
			 * the performance impact is assumed to be negligible
			 */
			/* TODO merge this with the similar code in the
			 * constructor */
			let state = {}
			for (let pair of window.location.hash.substr(1).split('&')) {
				let [key, value] = pair.split('=')
				/* Only allow variables already defined in the state */
				if (this.state[key] !== undefined) {
					state[key] = parse_type(key, value);
				}
			}
			this.setState(state);
		});
	}

	change = (name, value) => {
		let dict = {};
		let newval;
		if (name == 'foreground' || name == 'background')
			newval = value;
		else
			newval = +value;
		dict[key] = newval;
		this.setState(dict);

		let xs = [];
		for (var key in this.state) {
			/* Special case since this.state may be changed async. */
			let val = key == name ? newval : this.state[key];
			xs.push(`${key}=${val}`);
		}

		window.location.hash = xs.join('&');
	}

	render () {
		return (
			<div>
			<Logo {...this.state}/>
			<Controlls change={this.change} {...this.state}/>
			</div>
		)
	}
}


window.onload = function () {
	ReactDOM.render(<BuildALogo/>, document.getElementById("logo"));
}

window.download_logo = function download_logo() {
	let data = document.getElementById('thelogo').outerHTML;
	trigger_download (data);
}
